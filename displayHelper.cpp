#include "displayHelper.h"
#include "pinDefinitions.h"
#include "logo.h"
#include <TFT_eSPI.h>

#ifndef TFT_DISPOFF
#define TFT_DISPOFF 0x28
#endif

#ifndef TFT_SLPIN
#define TFT_SLPIN   0x10
#endif

#define TFT_MOSI            19
#define TFT_SCLK            18
#define TFT_CS              5
#define TFT_DC              16
#define TFT_RST             23

#define TFT_BL          4  // Display backlight control pin
#define ADC_EN          14
#define ADC_PIN         34
#define BUTTON_1        35
#define BUTTON_2        0


/*
    This project uses the tft_espi display by Bodmer to log directly to tft a output screen.
    https://github.com/Bodmer/TFT_eSPI
*/

TFT_eSPI display = TFT_eSPI(135, 240); // Invoke custom library and set resolution

void resetScreen() {
    // cursor at beginning and select font 1
    display.setCursor(0, 0);

    // make text color black on white background
    display.fillScreen(TFT_WHITE);
    display.setTextColor(TFT_BLACK,TFT_WHITE);
}

void startTFTDisplay() {
    display.init(); // begin session with Bodmer's display_espi library

    // set portrait mode
    display.setRotation(0);

    resetScreen();
    display.setTextSize(1);

    //strings are printed relative to the bottom left of screen
    display.setTextDatum(TL_DATUM);

    if (TFT_BL > 0) { // TFT_BL has been set in the TFT_eSPI library in the User Setup file TTGO_T_Display.h
         pinMode(TFT_BL, OUTPUT); // Set backlight pin to output mode
         digitalWrite(TFT_BL, TFT_BACKLIGHT_ON); // Turn backlight on
    }

    display.setSwapBytes(true);
    display.pushImage(0, 0,  240, 135, logo);

}

void drawString(const char* string, int offset) {
    display.drawString(string, 15, (display.height() / 2) - offset );
}
