# BLE IPS Server

## System Configuration
- Working Copy of Arduino IDE. This project uses v.1.8.8
- Visual Studio Code IDE with the Arduino extension
- Optional: TFT_ESPI Arduino Library from Bodmer, place in default lib path. IMPORTANT: when actually using this library, make sure to uncomment the include line in the User_Setup.h file in /home/$USER/Arduino/libraries/TFT_eSPI/ or wherever it is on windows.

## Project setup

```sh
# Clone this project from Gitlab to your computer
git clone git@gitlab.com:mark-matura/ble-ips-server.git

# open cloned server project folder in vscode
code ble-ips-server
```
## Arduino Extension configuration for VS Code
Disregard this paragraph if you have chosen to use a different IDE. This is only for configuring the extension that interfaces with the Arduino IDE.

Configure IDE for your board of your choice by presing CTRL + SHIFT + P and then selecting Configure Arduino Board Manager. Initialize a new Arduino conf file with CTRL + SHIFT + P and then "Arduino: Initialize". This creates a new .vscode folder to contain the editor configurations. Make a new directory in the project folder called "build" or something similar and add this line to your `arduino.json`:

```json
    "output": "build"
```
This line instructs the compiler to cache compiled files here. With separate compilation enabled the build time is improved substantially.

My sample arduino.json for reference:
```json
{
    "board": "esp32:esp32:esp32",
    "configuration": "PSRAM=disabled,PartitionScheme=default,CPUFreq=240,FlashMode=qio,FlashFreq=80,FlashSize=4M,UploadSpeed=921600,DebugLevel=none",
    "port": "COM7",
    "sketch": "ble-ips-server.ino",
    "output": "build"
}
```

## Compiler Configuration for VS Code

Update the paths for your System in the .vscode/c_cpp_properties.json file. Specify the necessary includePaths for the linker to find custom libraries such as the ble-ips-lib core headers.

For the details on setting up a c_cpp_properties.json file, have a look at the [the docs](https://code.visualstudio.com/docs/cpp/c-cpp-properties-schema-reference)


## Reference c_cpp_properties.json files
### Linux

```json
{
    "configurations": [
        {
            "name": "Linux",
            "includePath": [
                "/home/$USER/.arduino15/packages/esp32/tools/**",
                "/home/$USER/.arduino15/packages/esp32/hardware/esp32/1.0.4/**",
                "/home/$USER/Arduino/libraries/TFT_eSPI",
                "${workspaceFolder}/**"
            ],
            "forcedInclude": [
                "${workspaceFolder}/displayHelper.h",
                "${workspaceFolder}/env.h"
            ],
            "browse": {
                "path": [
                    "${workspaceFolder}"
                ]
            },
            "intelliSenseMode": "gcc-x64",
            "compilerPath": "/usr/bin/gcc",
            "cStandard": "c11",
            "cppStandard": "gnu++14"
        }
    ],
    "version": 4
}
```

### Windows
```json
{
    "configurations": [
        {
            "name": "Win32",
            "includePath": [
                "${workspaceFolder}",
                "C:\\Users\\marmar\\AppData\\Local\\Arduino15\\packages\\esp32\\tools\\**",
                "C:\\Users\\marmar\\AppData\\Local\\Arduino15\\packages\\esp32\\hardware\\esp32\\1.0.4\\**",
                "C:\\Program Files (x86)\\Arduino\\hardware\\tools\\**",
                "C:\\Users\\marmar\\Documents\\Arduino\\libraries\\**",
                "${workspaceFolder}"
            ],
            "browse": {
                "path": [
                    "${workspaceFolder}",
                    "C:\\Users\\marmar\\AppData\\Local\\Arduino15\\packages\\esp32\\tools\\**",
                    "C:\\Users\\marmar\\AppData\\Local\\Arduino15\\packages\\esp32\\hardware\\esp32\\1.0.4\\**",
                    "C:\\Users\\marmar\\Documents\\Arduino\\libraries\\**",
                    "${workspaceFolder}",
                    "/usr/local/include"
                ]
            },
            "forcedInclude": [
                "${workspaceFolder}\\displayHelper.h",
                "${workspaceFolder}\\env.h"
            ],
            "intelliSenseMode": "clang-x64",
            "compilerPath": "/usr/bin/gcc",
            "cStandard": "c11",
            "cppStandard": "c++17"
        }
    ],
    "version": 4
}
```


```json
{
    "board": "esp32:esp32:esp32",
    "configuration": "PSRAM=disabled,PartitionScheme=default,CPUFreq=240,FlashMode=qio,FlashFreq=80,FlashSize=4M,UploadSpeed=115200,DebugLevel=none",
    "sketch": "ble-ips-server.ino",
    "port": "/dev/ttyUSB0",
    "output": "build"
}
```

You should be all set now, go ahead and build + upload the project using the button in the top corner of the GUI or by pressing CTRL + ALT + U