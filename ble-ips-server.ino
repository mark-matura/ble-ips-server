#include <BLEDevice.h>

#include "displayHelper.h"
#include "env.h"



BLEAdvertising *advertiser;
int advertisingIntervalTarget = 100;

/*
	Based on the example sketch from arduino ide BLE_server.ino and the ttgo t-display example file from Lilygo

	https://github.com/nkolban/ESP32_BLE_Arduino/blob/master/examples/BLE_server/BLE_server.ino
*/

/**
 * @brief Initialize BLE, and advertise a service with shared UUID
 *
 * @param void
 * @return void
 */
void setupBeacon() {
    BLEDevice::init("BLE IPS Server");

    // https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/bluetooth/controller_vhci.html#_CPPv414ESP_PWR_LVL_N3
    //  set tx power level to -3 dBm
    BLEDevice::setPower(ESP_PWR_LVL_N3);

    // set up fake service to be advertised, client will try to match the service UUID
    advertiser = BLEDevice::getAdvertising();
    advertiser->addServiceUUID(SERVICE_UUID);

    advertiser->setMinInterval(advertisingIntervalTarget - 20);
    advertiser->setMaxInterval(advertisingIntervalTarget + 20);
}

void setup() {
    startTFTDisplay();
    setupBeacon();

    drawString(BLEDevice::getAddress().toString().c_str(), 30);

    advertiser->start();
}

void loop() {
}
